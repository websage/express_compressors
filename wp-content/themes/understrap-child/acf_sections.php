<?php
// Get Image Function
function ws_get_image( $param ){
    switch ($param['type']) {
        case 'img-responsive':
            if ($param['id']) {

                if(isset($param['lightbox']) == true){
                    if(isset($param['lightboximageid'])) {
                        $lightboxid = $param['lightboximageid'];
                    }
                    else {
                        $lightboxid = $param['id'];
                    }

                    if(isset($param['extraclasses'])){
                        $classes = $param['extraclasses'] . ' img-responsive';
                    }
                    else {
                        $classes = 'img-responsive';
                    }

                    $image_array = array(
                        'class' => $classes,
                        'alt' => isset($param['title']),
                        'title' => isset($param['title']),
                    );
                }
                else {

                    if(isset($param['extraclasses'])){
                        $classes = $param['extraclasses'] . ' img-responsive';
                    }
                    else {
                        $classes = 'img-responsive';
                    }

                    $image_array = array(
                        'class' => $classes,
                        'alt' => isset($param['title']),
                        'title' => isset($param['title']),
                    );
                }

                $image = wp_get_attachment_image($param['id'], $param['size'], false, $image_array);


            } else {

                if(isset($param['extraclasses'])){
                    $classes = $param['extraclasses'] . ' img-responsive';
                }
                else {
                    $classes = 'img-responsive';
                }

                $image = wp_get_attachment_image(get_field('ws_default_image', 'options')['ID'], $param['size'], false, array(
                    'class' => $classes,
                    'alt' => 'Default Image',
                    'title' => 'Default Image',
                ));
            }

            return $image;

            break;

        case 'background':


            if ($param['id']) {
                $image = wp_get_attachment_image_src($param['id'], $param['size'])[0];
            } else {

                $default = get_field('ws_default_image', 'options');
                $image = wp_get_attachment_image_src($default['ID'], 'full')[0];

            }

            return $image;

            break;

        case 'None':

            $image = wp_get_attachment_image(get_field('ws_default_image', 'options'), $param['size'], false, array(
                'class' => 'img-responsive',
                'alt' => 'Default Image',
                'title' => 'Default Image',
            ));

            return $image;

            break;
    }
}

// Set the Bootstrap Column Fuction

function ws_get_boostrap_col($ID, $args = array()){

    $prefix = $args['prefix'];


    if (!empty($prefix)) {
        $args_default = array(
            'desktop_row'   => get_sub_field($prefix . 'desktop_row', $ID),
            'tablet_row'    => get_sub_field($prefix . 'tablet_row', $ID),
            'mobile_row'    => get_sub_field($prefix . 'mobile_row', $ID),
        );
    }
    else {
        $args_default = array(
            'desktop_row'   => 4,
            'tablet_row'    => 2,
            'mobile_row'    => 1,
        );
    }

    $args = array_replace_recursive( $args_default, $args );

    $bootstrap_cols = array(
        'col-md-'   => (12 / $args['desktop_row']),
        'col-sm-'    => (12 / $args['tablet_row']),
        'col-xs-'    => (12 / $args['mobile_row']),
    );

    foreach ($bootstrap_cols as $key => $value) {
        $output[] = $key . $value;
    }

    return $output;

}

// ACF Content Sections Selection

add_action('ws_content_sections', 'ws_content_sections_init');
function ws_content_sections_init($params = array())
{
    extract($params);
    $section_count = 0;

    while(have_rows('ws_content_sections', $ID)) {
        the_row();
        $current_layout = get_row_layout();

        // GALLERY
        if($current_layout == 'ws_gallery'):
            switch (get_sub_field('ws_gallery_type')) {
                case 'grid':
                    include(ws_get_template_part('layout', 'gallery'));
                    break;

                default:
                    include(ws_get_template_part('layout', 'gallery'));
                    break;
            }
        endif;

        // SECTION BREAK
        if($current_layout == 'ws_sectionbreak'):
            include(ws_get_template_part('layout', 'section_break'));
        endif;

        // SIMPLE PRODUCT
        if($current_layout == 'ws_product_gallery'):
            switch (get_sub_field('ws_product_gallery_type')) {
                case 'grid':
                    include(ws_get_template_part('layout', 'simple_products'));
                    break;

                default:
                    include(ws_get_template_part('layout', 'simple_products'));
                    break;
            }
        endif;

        // CONTENT BLOCK
        if ($current_layout == 'ws_content_block'):
            if (get_sub_field('ws_content_item')):
                switch (get_sub_field('ws_content_block_type')) {
                    case 'standard':
                        include(ws_get_template_part('layout', 'content_block'));
                        break;

                    case 'custom':

                        $prefix = get_sub_field('ws_custom_layout_prefix');
                        $suffix = get_sub_field('ws_custom_layout_suffix');

                        if ($td_template_part = ws_get_template_part($prefix, $suffix)) {
                            include($td_template_part);
                        }
                        break;

                    default:
                        include(ws_get_template_part('layout', 'content_block'));
                        break;
                }
            endif;
        endif;


        // TITLE
        if ($current_layout == 'ws_section_header'):
            include(ws_get_template_part('layout', 'section_title'));
        endif;

        // ACCORDION
        if ($current_layout == 'ws_accordion'):
            include(ws_get_template_part('layout', 'accordion'));
        endif;

        // TESTIMONIAL
        if ($current_layout == 'ws_testimonials'):
            include(ws_get_template_part('layout', 'testimonials'));
        endif;

        // TABLE
        if ($current_layout == 'ws_table_data'):
            include(ws_get_template_part('layout', 'table'));
        endif;

        // CPT
        if ($current_layout == 'ws_cpt_listing_layout'):
            include(ws_get_template_part('layout', 'cpt'));
        endif;


        // CUSTOM
        if ($current_layout == 'ws_custom_layout'):
            $prefix = get_sub_field('ws_custom_layout_prefix');
            $suffix = get_sub_field('ws_custom_layout_suffix');

            if ($td_template_part = ws_get_template_part($prefix, $suffix)) {
                include($td_template_part);
            }
        endif;

        $section_count++;
    };

}

function ws_get_template_part( $slug, $name = '') {
    $template = '';

    // Look in yourtheme-child/layouts/slug-name.php
    if ( $name && file_exists( get_stylesheet_directory() . '/layouts/' . "{$slug}-{$name}.php" ) ) {
        $template = get_stylesheet_directory() . '/layouts/' . "{$slug}-{$name}.php";
    }

    // Look in yourtheme/layouts/slug-name.php
    if (  ! $template && $name && file_exists( get_template_directory() . '/layouts/' . "{$slug}-{$name}.php" ) ) {
        $template = get_template_directory() . '/layouts/' . "{$slug}-{$name}.php";
    }

    return $template;
}

function ws_get_section_part( $slug, $name = '') {
    $template = '';

    // Look in yourtheme-child/layouts/slug-name.php
    if ( $name && file_exists( get_stylesheet_directory() . '/sections/' . "{$slug}-{$name}.php" ) ) {
        $template = get_stylesheet_directory() . '/sections/' . "{$slug}-{$name}.php";
    }

    // Look in yourtheme/layouts/slug-name.php
    if (  ! $template && $name && file_exists( get_template_directory() . '/sections/' . "{$slug}-{$name}.php" ) ) {
        $template = get_template_directory() . '/sections/' . "{$slug}-{$name}.php";
    }

    return $template;
}