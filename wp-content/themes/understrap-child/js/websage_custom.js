jQuery(document).ready(function() {
    // Accordion Functions
    jQuery('#accordion').find('.accordion-toggle').click(function(){

        jQuery(".accordion-toggle.current").removeClass('current');

        //Expand or collapse this panel
        jQuery(this).next().slideToggle('fast');
        jQuery(this).addClass("current")

        //Hide the other panels
        jQuery(".accordion-content").not(jQuery(this).next()).slideUp('fast');

    });
    jQuery( ".accordion-toggle" ).first().click();
});