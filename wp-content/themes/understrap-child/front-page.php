<?php
/**
 * The template for displaying the front page.
 *
 *
 * @package understrap
 */
$ID = get_the_ID();

get_header();

$container   = get_theme_mod( 'understrap_container_type' );
$sidebar_pos = get_theme_mod( 'understrap_sidebar_position' );

?>

<div class="wrapper" id="page-wrapper">
        <div class="additional-content">
            <?php
            do_action('ws_content_sections', array('ID' => $ID));
            ?>
        </div>


<?php the_field('custom_javascript'); ?>

<?php get_footer(); ?>
