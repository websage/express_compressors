<div class="header_search">
    <div class="form-group">
        <div class="icon-addon addon-md">
            <form role="search" method="get" class="search-form clearfix" action="<?php echo home_url( '/' ); ?>">
                <input type="text" value="Search" class="form-control" onfocus="if( this.value == this.defaultValue ) this.value = ''" onblur="if( this.value == '' ) this.value = this.defaultValue" name="s" id="s" />
            </form>
        </div>
    </div>
</div>