<?php
/**
 */

if (have_rows('ws_schema_type', $ID)) {
    while (have_rows('ws_schema_type', $ID)) {

        the_row();

        $current_layout = get_row_layout();

        // Product JSON-LD
        if ($current_layout == 'ws_json_ld_product'){

            $brand = get_sub_field('ws_json_ld_product_brand');
            $name = get_sub_field('ws_json_ld_product_name');
            $image = get_sub_field('ws_json_ld_product_image');
            $description = get_sub_field('ws_json_ld_product_description');

            $product_schema = "<script type='application/ld+json'>";
            $product_schema = $product_schema . '{"@context": "http://www.schema.org","@type": "product",';
            $product_schema = $product_schema . '"brand": "' . $brand . '",';
            $product_schema = $product_schema . '"name": "' . $name . '",';
            $product_schema = $product_schema . '"image": "' . $image . '",';
            $product_schema = $product_schema . '"description": "' . $description . '"}</script>';

            echo $product_schema;

        }
    }
}