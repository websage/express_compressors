<?php
/**
Template Name: Search Page
 *
 *
 * @package understrap
 */
$ID = get_the_ID();

get_header();

$container   = get_theme_mod( 'understrap_container_type' );
$sidebar_pos = get_theme_mod( 'understrap_sidebar_position' );

?>

<div class="wrapper" id="page-wrapper">
    <div class="container breadcrumbs"><div class="row">
            <div class="col-md-12"><?php  bcn_display(); ?></div>
        </div> </div>

    <div class="content__block container">

        <section>
            <div class='row row-eq-height'>

    <?php
    if ( have_posts() ) : while ( have_posts() ) : the_post();
    ?>

    <div class="search__item search__item col-md-12 col-sm-12 col-xs-12">

        <div class="search__item__content">
            <div class="search_vert_listing_item">

            <?php

                echo "<h3 class='search__link'><a href='" . get_permalink() . "'>" . get_the_title() . "</a></h3>";
            echo "<span>" . get_the_excerpt() . "</span>";

            ?>

            </div>


        </div>
    </div>

    <?php

    endwhile;

    endif;


    ?>

            </div>

        </section>

    </div>

    <!-- Wrapper end -->
    <?php the_field('custom_javascript'); ?>

    <?php get_footer(); ?>