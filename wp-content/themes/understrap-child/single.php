<?php
/**
 * Template Name: Archive Single
 *
 *
 * @package WordPress
 * @subpackage Web Sage Theme
 * @since 1.0
 */
$ID = get_the_ID();

get_header();

$container = get_theme_mod('understrap_container_type');
$sidebar_pos = get_theme_mod('understrap_sidebar_position');

$featured_image = ws_get_image( array("type"=>"img-responsive","id"=>get_post_thumbnail_id(),"size"=>'full_width', "title"=>get_post_meta( get_post_thumbnail_id(), '_wp_attachment_image_alt', true)));
$featured_image_caption = get_post(get_post_thumbnail_id())->post_excerpt;

?>

<div class="wrapper" id="page-wrapper">
    <div class="container breadcrumbs"><div class="row">
            <div class="col-md-12"><?php  bcn_display(); ?></div>
        </div> </div>

    <?php
    if ( have_posts() ) : while ( have_posts() ) : the_post();

        if(get_the_content()){
            ?>
            <div class="product-content pb30">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">

                            <span class="featured_image"><?php echo $featured_image; ?></span>
                            <?php if($featured_image_caption) { ?>
                                <span class="featured_caption"><?php echo $featured_image_caption; ?></span>
                            <?php } ?>

                            <?php
                            the_content();
                            ?>
                        </div>
                    </div>
                </div>

            </div>
            <?php
        }

    endwhile;


    else:

    endif;
    ?>


    <div class="additional-content">
        <?php
        do_action('ws_content_sections', array('ID' => $ID));
        ?>
    </div>

    <!-- Wrapper end -->
    <?php the_field('custom_javascript'); ?>

    <?php get_footer(); ?>
