<?php
/**
 * Section Template Header Inner PAges
 */

$logo = get_field('ws_logo', 'options');
$companyname = get_field('ws_business_name', 'options');

if(get_field('ws_title_background')){
    $title_background = ws_get_image( array("type"=>"background","id"=>get_field('ws_title_background'),"size"=>'product'));
}
else {
    $title_background = ws_get_image( array("type"=>"background","id"=>get_field('title_page_background', 'option'),"size"=>'full'));
}

?>

<div class="header_area">
    <div class="container">
        <div class="row">
            <div class="col-md-2 col-sm-2 col-xs-12">
                <div class="site_logo">
                    <a href="/"><?php echo ws_get_image(array("type" => "img-responsive", "id" => $logo['id'], "size" => 'full', "title" => $companyname, "extraclasses" => '')); ?></a>
                </div>
            </div>
            <div class="col-md-10 col-sm-10 col-xs-12">
                <div class="header_right">
                    <?php get_search_form(); ?>
                    <div class="site_nav">

                        <nav class="navbar navbar-default">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#mainNav">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                            <div class="collapse navbar-collapse" id="mainNav">
                                <!-- The WordPress Menu goes here -->
                                <?php wp_nav_menu(array(
                                        'menu' => 'primary',
                                        'theme_location' => 'primary',
                                        'depth' => 2,
                                        'menu_class' => 'nav navbar-nav',
                                        'fallback_cb' => 'WP_Bootstrap_Navwalker::fallback',
                                        'walker' => new WP_Bootstrap_Navwalker())
                                ); ?>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<section id="page__title" style="background: url('<?php echo $title_background;  ?>') no-repeat; background-size: cover; background-position-y: 50%;">
    <div class="titlebackground">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="title_heading">
                    <h1><?php
                        if(is_home()){
                            echo "News";
                        }
                        else {
                            if(is_404()){
                                echo "404 Page Not Found";
                            }
                            elseif(is_search()){
                                echo "Search Results";
                            }
                            elseif(is_archive()){
                                echo "News";
                            }
                            else {
                                echo get_the_title();
                            }
                        }
                        ?></h1>
                    <div class="title_bar"></div></div>
                </div>
            </div>
        </div>
    </div>
</section>