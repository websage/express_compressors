<?php
/**
 * Section Template Header Frontpage
 */

$logo = get_field('ws_logo', 'options');
$menulogo = get_field('ws_menu_logo', 'options');
$companyname = get_field('ws_business_name', 'options');

?>

<div class="header__area">
    <div class="container">
        <div class="row">
            <div class="col-md-2 col-sm-2 col-xs-12">
                <div class="site_logo">
                    <a href="/"><?php echo ws_get_image(array("type" => "img-responsive", "id" => $logo['id'], "size" => 'full', "title" => $companyname, "extraclasses" => '')); ?></a>
                </div>
            </div>
            <div class="col-md-10 col-sm-10 col-xs-12">
                <div class="header_right">
                    <?php get_search_form(); ?>
                    <div class="site_nav">

                        <nav class="navbar navbar-default">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#mainNav">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                            <div class="collapse navbar-collapse" id="mainNav">
                                <!-- The WordPress Menu goes here -->
                                <?php wp_nav_menu(array(
                                        'menu' => 'primary',
                                        'theme_location' => 'primary',
                                        'depth' => 2,
                                        'menu_class' => 'nav navbar-nav',
                                        'fallback_cb' => 'WP_Bootstrap_Navwalker::fallback',
                                        'walker' => new WP_Bootstrap_Navwalker())
                                ); ?>
                            </div>
                        </nav>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<div id="home__slider">
    <div class="container-fluid pad0 m0">
        <div class="row pad0 m0">
            <div class="col-lg-12 pad0 m0">
                <?php
                    echo do_shortcode('[slick-slider design="prodesign-17" show_content="false" category="412" orderby="menu_order" order="ASC"]');
                ?>
            </div>
        </div>
    </div>
</div>