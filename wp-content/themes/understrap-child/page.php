<?php
/**
 * The template for displaying the front page.
 *
 *
 * @package understrap
 */
$ID = get_the_ID();

get_header();

$container   = get_theme_mod( 'understrap_container_type' );
$sidebar_pos = get_theme_mod( 'understrap_sidebar_position' );

?>

<div class="wrapper" id="page-wrapper">
    <div class="container breadcrumbs"><div class="row">
            <div class="col-md-12"><?php  bcn_display(); ?></div>
        </div> </div>

    <?php
    if ( have_posts() ) : while ( have_posts() ) : the_post();

    if(get_the_content()){
    ?>
    <div class="page-content pb30">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <?php
                        the_content();
                    ?>
                </div>
            </div>
        </div>

    </div>
    <?php
    }

    endwhile;


    else:

    endif;
    ?>



        <div class="additional-content">
            <?php
            do_action('ws_content_sections', array('ID' => $ID));
            ?>
        </div>

    <!-- Wrapper end -->
    <?php the_field('custom_javascript'); ?>

    <?php get_footer(); ?>
