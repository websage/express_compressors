<?php
/**
 * The template for displaying the 404 page.
 *
 *
 * @package understrap
 */
$ID = get_the_ID();

get_header();

?>

<div class="wrapper" id="page-wrapper">

    <div class="page-content pb70 pt70">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p>It looks like nothing was found at this location. Please try one on the links below.</p>
                    <?php echo do_shortcode('[wp_sitemap_page] ');  ?>
                </div>
            </div>
        </div>

    </div>


    <!-- Wrapper end -->
    <?php the_field('custom_javascript'); ?>

    <?php get_footer(); ?>
