<?php
/**
 * The template for displaying the front page.
 *
 *
 * @package understrap
 */
$ID = get_the_ID();

get_header();

$container = get_theme_mod('understrap_container_type');
$sidebar_pos = get_theme_mod('understrap_sidebar_position');

?>

<div class="wrapper" id="page-wrapper">
    <div class="container breadcrumbs">
        <div class="row">
            <div class="col-md-12"><?php bcn_display(); ?></div>
        </div>
    </div>
    <div class="content__block container post_listing">
        <section>
            <div class="row row-eq-height">

                <?php
                if (have_posts()) : while (have_posts()) : the_post();

                    if (get_the_content()) {
                        ?>
                        <div class="cpt__item cpt__item-1 col-md-4 col-sm-6 col-xs-12 cpt__item-post">

                            <div class="cpt__item__content">

                                <div class="single_news">
                                    <a href="<?php echo get_permalink(); ?>"><?php echo ws_get_image(array("type" => "img-responsive", "id" => get_post_thumbnail_id(), "size" => 'item_listing')); ?></a>
                                    <div class="news_inner">
                                        <a href="<?php echo get_permalink(); ?>"><h3><?php echo get_the_title(); ?></h3>
                                        </a>
                                        <span><?php echo substr(wp_strip_all_tags(get_the_content()), 0, 300); ?></span>
                                        <div class="center">
                                        <a href="<?php echo get_permalink(); ?>" class="btn btn-primary">FIND OUT
                                            MORE</a></div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <?php
                    }

                endwhile;


                    echo "<div class='ctp__paging'><nav class='paging col-md-12'>";
                    previous_posts_link( 'Previous Page' );
                    next_posts_link( 'Next Page' );
                    echo "</nav></div>";



                else:

                endif;
                ?>
            </div>
        </section>
    </div>

    <!-- Wrapper end -->
    <?php the_field('custom_javascript'); ?>

    <?php get_footer(); ?>
