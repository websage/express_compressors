<?php
function understrap_remove_scripts() {
    wp_dequeue_style( 'understrap-styles' );
    wp_deregister_style( 'understrap-styles' );

    wp_dequeue_script( 'understrap-scripts' );
    wp_deregister_script( 'understrap-scripts' );

    // Removes the parent themes stylesheet and scripts from inc/enqueue.php
}
add_action( 'wp_enqueue_scripts', 'understrap_remove_scripts', 20 );

// Async load
function ikreativ_async_scripts($url)
{
    if ( strpos( $url, '#asyncload') === false )
        return $url;
    else if ( is_admin() )
        return str_replace( '#asyncload', '', $url );
    else
        return str_replace( '#asyncload', '', $url )."' async='async";
}
add_filter( 'clean_url', 'ikreativ_async_scripts', 11, 1 );


add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {

	// Get the theme data
	$the_theme = wp_get_theme();

    wp_enqueue_style( 'child-understrap-styles', get_stylesheet_directory_uri() . '/css/child-theme.min.css', array(), $the_theme->get( 'Version' ) );
    wp_enqueue_script( 'jquery' );
	wp_enqueue_script( 'popper-scripts', get_template_directory_uri() . '/js/popper.min.js', array(), false);
    wp_enqueue_script( 'child-understrap-scripts', get_stylesheet_directory_uri() . '/js/child-theme.min.js', array(), $the_theme->get( 'Version' ), true );

    wp_register_script('ws-custom', get_stylesheet_directory_uri() . '/js/websage_custom.min.js#asyncload', array(), '', true);
    wp_enqueue_script('ws-custom');

    // Lightbox Styles and Scripts
    wp_enqueue_style( 'featherlight-styles', get_stylesheet_directory_uri() . '/css/featherlight.min.css', array(), $the_theme->get( 'Version' ) );
    wp_register_script('featherlight-scripts', get_stylesheet_directory_uri() . '/js/featherlight.min.js', array(), '', true);
    wp_enqueue_style( 'featherlight-gallery-styles', get_stylesheet_directory_uri() . '/css/featherlight.gallery.min.css', array(), $the_theme->get( 'Version' ) );
    wp_register_script('featherlight-gallery-scripts', get_stylesheet_directory_uri() . '/js/featherlight.gallery.min.js', array(), '', true);
    wp_enqueue_script('featherlight-scripts');
    wp_enqueue_script('featherlight-gallery-scripts');

}

// Enable ACF Options Page
if (function_exists('acf_add_options_page')) {
    acf_add_options_page(array(
        'page_title'    => 'Website Options',
        'capability'    => 'edit_posts',
        'icon_url'      => 'dashicons-list-view',
    ));

    acf_add_options_page(array(
        'page_title'    => 'Admin Options',
        'capability'    => 'manage_options',
        'icon_url'      => 'dashicons-admin-settings',
    ));

    acf_add_options_page(array(
        'page_title'    => 'Item Template Options',
        'capability'    => 'manage_options',
        'icon_url'      => 'dashicons-admin-settings',
    ));


}
add_action('init', 'ws_add_custom_image_sizes', 1);


// Add Image Sizes
function ws_add_custom_image_sizes() {
    add_image_size( 'featured', 700, 530, true);
    add_image_size( 'post_single', 850, 425, true);
    add_image_size( 'gallery_thumb', 400, 300, true);
}

// Strip out all characters except for numbers
function ws_numbersonly($myvar)
{
    return preg_replace("/\D/", "",$myvar);
}

require_once __DIR__ . '/acf_sections.php';
require_once __DIR__ . '/cpt_setup.php';
require_once __DIR__ . '/tinymice_custom_styles.php';