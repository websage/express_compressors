<?php
/**
 * Template for showing Content Blocks
 */

if (!defined('ABSPATH'))
    exit; // Exit if accessed directly

$block_extra_classes = get_sub_field('ws_table_block_extra_classes');
$table_border = get_sub_field('ws_table_border');
$table_padding = get_sub_field('ws_table_padding');

if($table_border){
    $table_border_output = "border='" . $table_border . "'";
}
?>


<div class="content__block <?php if($block_extra_classes){ echo $block_extra_classes; }  ?>">
    <section>
        <div class='row row-eq-height'>
        <div class="col-xs-12 table__item">
            <?php

            $table = get_sub_field( 'ws_table' );

            if ( $table ) {

                echo '<table ' . $table_border_output . '>';

                if ( $table['header'] ) {

                    echo '<thead>';

                    echo '<tr>';

                    foreach ( $table['header'] as $th ) {

                        echo '<th>';
                        echo $th['c'];
                        echo '</th>';
                    }

                    echo '</tr>';

                    echo '</thead>';
                }

                echo '<tbody>';

                foreach ( $table['body'] as $tr ) {

                    echo '<tr>';

                    foreach ( $tr as $td ) {

                        echo '<td>';
                        echo $td['c'];
                        echo '</td>';
                    }

                    echo '</tr>';
                }

                echo '</tbody>';

                echo '</table>';
            }

            ?>
        </div>
    </div>
</div>





