<?php
/**
 * Template for showing Content Blocks
 */

if (!defined('ABSPATH'))
    exit; // Exit if accessed directly

if (have_rows('ws_content_item', $ID)) {
    while(have_rows('ws_content_item', $ID)){ the_row();

        $title = get_sub_field('ws_ci_title');
        $subtitle = get_sub_field('ws_ci_sub_title');
        $content = get_sub_field('ws_ci_content');
        $read_more = get_sub_field('ws_ci_read_more_text');
        $extra_classes = get_sub_field('extra_classes');

        switch (get_sub_field('ws_ci_link_type')) {
            case 'internal':
                $link_url = get_sub_field('ws_ci_internal_link');
                $link_attr = array(
                    'href'      => $link_url,
                    'role'      => 'button',
                    'class'     => $args['link_class'],
                    'data-mh'   => 'leadins-link',
                );

                break;

            case 'external':
                $link_url = get_sub_field('ws_ci_external_link');
                $link_attr = array(
                    'target'    => '_blank',
                    'rel'       => 'noreferrer',
                    'href'      => $link_url,
                    'role'      => 'button',
                    'class'     => $args['link_class'],
                    'data-mh'   => 'leadins-link',
                );
                break;

            case 'download':
                $link_url = $file_details['url'];
                $file_details = get_sub_field('ws_ci_download_link');
                $link_attr = array(
                    'target'    => '_blank',
                    'rel'       => 'noreferrer',
                    'href'      => $file_details['url'],
                    'role'      => 'button',
                    'class'     => $args['link_class'],
                    'data-mh'   => 'leadins-link',
                );
                break;

            case 'none':
                $link_url = '';
                $link_attr = array();
                $link_attr_image = array();
                break;
        }

        $loop_output[] = array(
            'sectionid'        => $section_count,
            'title'            => $title,
            'subtitle'         => $subtitle,
            'content'          => $content,
            'read_more'        => $read_more,
            'wrapper_class'    => $extra_classes,
        );


    }

    //var_dump($section_count);

    $output = json_decode(json_encode($loop_output), FALSE);

    $bootstrap_classes = implode(' ', ws_get_boostrap_col($ID, array('prefix' => 'ws_content_block_')));
}

?>


<div class="single__content__block container thin_width">
    <div class="row">

        <?php
        $block_count = 1;

        foreach ($output as $block) {
            if ($block->sectionid >= $section_count) {
                ?>

                <div class="single_block__item block__item-<?php echo $block_count; ?> <?php echo $block->wrapper_class; ?> col-md-12">
                    <div>

                        <?php
                        // Content Section
                        if ($block->content) {
                            ?>

                            <div class="single_block__item__content">
                                <?php
                                echo $block->content;
                                ?>
                            </div>

                            <?php
                        } // Image Section
                        ?>

                    </div>


                </div>


                <?php
                $block_count++;
            }
        }  // END FOREACH ?>

    </div>
</div>
