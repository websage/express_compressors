<?php
/**
 * Template for showing CPT Listings
 */

if (!defined('ABSPATH'))
    exit; // Exit if accessed directly

// Global Variables
global $wp_query, $paged;

$block_extra_classes = get_sub_field('ws_cpt_block_extra_classes');
$container_type = get_sub_field('ws_cpt_block_container_type');

$itemhtml = get_sub_field('items_custom_html');

$content_length = get_sub_field('ws_cpt_content_length');

if ( get_query_var('paged') ) { $paged = get_query_var('paged'); } else if ( get_query_var('page') ) {$paged = get_query_var('page'); } else {$paged = 1; }


// WP_Query arguments
$args = array(
    'post_type'              => array( get_sub_field('ws_cpt_post_type') ),
    'order'                  => 'DESC',
    'orderby'                => get_sub_field('ws_cpt_order_by'),
);

// Number of Posts Per Page
if(get_sub_field('ws_limit_number_of_posts')){
    $args['posts_per_page']=get_sub_field('ws_limit_number_of_posts');
    $args['nopaging']=false;
    $args['paged']=$paged;
}
else {
    $args['nopaging']=true;
}

// The Query
$query = new WP_Query( $args );

$bootstrap_classes = implode(' ', ws_get_boostrap_col($ID, array('prefix' => 'ws_content_block_')));
$myrowcount = get_sub_field('ws_content_block_desktop_row');



?>


<div class="content__block <?php echo $container_type . " "; echo get_sub_field('ws_cpt_post_type') . "_listing "; if($block_extra_classes){ echo $block_extra_classes; };  ?> ">

    <section>
        <div class='row row-eq-height'>

        <?php

        $block_count = 1;

        // The Loop
        if ( $query->have_posts() ) {

            while ( $query->have_posts() ) {

                $query->the_post();



                ?>


                <div class="cpt__item cpt__item-<?php echo $block_count; ?> <?php echo $bootstrap_classes; ?> cpt__item-<?php echo get_sub_field('ws_cpt_post_type'); ?>">

                    <div class="cpt__item__content">

                        <?php

                        if($itemhtml) {
                            $myHTML = $itemhtml;
                            $myHTML = str_replace("!TITLE!",get_the_title(),$myHTML);
                            if($content_length){
                                $myHTML = str_replace("!CONTENT!",preg_replace('/\s+?(\S+)?$/', '', substr(wp_strip_all_tags(get_the_content()), 0, $content_length)),$myHTML);
                                $myHTML = str_replace("!EXCERPT!",preg_replace('/\s+?(\S+)?$/', '', substr(wp_strip_all_tags(get_the_excerpt()), 0, $content_length))  . " ...",$myHTML);
                            }
                            else {
                                $myHTML = str_replace("!CONTENT!",wp_strip_all_tags(get_the_content()),$myHTML);
                                $myHTML = str_replace("!EXCERPT!",wp_strip_all_tags(get_the_excerpt()),$myHTML);
                            }
                            $myHTML = str_replace("!FEATURED!",ws_get_image( array("type"=>"img-responsive","id"=>get_post_thumbnail_id(),"size"=>'item_listing')),$myHTML);
                            $myHTML = str_replace("!BACKGROUND!",ws_get_image( array("type"=>"background","id"=>get_post_thumbnail_id(),"size"=>'item_listing')),$myHTML);
                            $myHTML = str_replace("!LINKURL!",get_permalink(),$myHTML);
                            echo $myHTML;
                        }
                        else {

                            echo "<a href='" . get_permalink() . "'>" . ws_get_image( array("type"=>"img-responsive","id"=>get_post_thumbnail_id(),"size"=>'item_listing')) . "</a>";
                            echo "<h4 class='cpt__link'><a href='" . get_permalink() . "'>" . get_the_title() . "</a></h4>";
                            if($content_length){
                                echo "<span>" . preg_replace('/\s+?(\S+)?$/', '', substr(wp_strip_all_tags(get_the_content()), 0, $content_length)) . " ..</span>";
                            }
                            else {
                                echo "<span>" . wp_strip_all_tags(get_the_content()) . "</span>";
                            }
                            if(get_sub_field('ws_cpt_read_more_text')){
                                echo "<a href='" . get_permalink() . "' class='readmore_link'>" . get_sub_field('ws_cpt_read_more_text') . "</a>";
                            }
                        }

                        ?>


                    </div>
                </div>


                <?php

                $block_count++;

            }

            if(get_sub_field('ws_include_paging')){
                echo "<div class='ctp__paging'><nav class='paging col-md-12'>";
                previous_posts_link( 'Previous Page' );
                next_posts_link( 'Next Page', $query->max_num_pages );
                echo "</nav></div>";
            }



        } else {
            // no posts found
        }


        ?>
        </div>

    </section>

</div>

<?php wp_reset_query(); ?>

<?php

// Restore original Post Data
wp_reset_postdata();

?>