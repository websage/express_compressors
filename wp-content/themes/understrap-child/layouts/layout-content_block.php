<?php
/**
 * Template for showing Content Blocks
 */

if (!defined('ABSPATH'))
    exit; // Exit if accessed directly

$block_extra_classes = get_sub_field('ws_content_block_extra_classes');
$container_type = get_sub_field('ws_content_block_container_type');
$itemshtml = get_sub_field('ws_ci_items_html');
$rowequalheight = get_sub_field('ws_ci_row_equal_height');
$block_background = get_sub_field('ws_section_ci_background_image');
$full_width_background = get_sub_field('ws_cb_full_width_background');

if($block_background){
    $block_background = ws_get_image( array("type"=>"background","id"=>$block_background['ID'],"size"=>'full'));
}

if (have_rows('ws_content_item', $ID)) {
    while(have_rows('ws_content_item', $ID)){ the_row();

        $title = get_sub_field('ws_ci_title');
        $content = get_sub_field('ws_ci_content');
        $html = get_sub_field('ws_ci_html');
        $btntext = get_sub_field('ws_ci_button_text');
        $image = get_sub_field('ws_ci_featured_image');
        $image_set = false;

        if($image){
            $image_set = true;
        }

        $cb_image = ws_get_image( array("type"=>"img-responsive","id"=>$image['ID'],"size"=>'full'));
        $cb_image_url = ws_get_image( array("type"=>"background","id"=>$image['ID'],"size"=>'full'));


        switch (get_sub_field('ws_ci_link_type')) {
            case 'internal':
                $link_url = get_sub_field('ws_ci_internal_link');
                $link_attr = array(
                    'href'      => $link_url,
                    'role'      => 'button',
                    'class'     => $btnclasss,
                    'text'   => $btntext,

                );

                break;

            case 'external':
                $link_url = get_sub_field('ws_ci_external_link');
                $link_attr = array(
                    'target'    => '_blank',
                    'rel'       => 'noreferrer',
                    'href'      => $link_url,
                    'role'      => 'button',
                    'class'     => $btnclasss,
                    'text'   => $btntext,
                );
                break;

            case 'download':
                $link_url = $file_details['url'];
                $file_details = get_sub_field('ws_ci_download_link');
                $link_attr = array(
                    'target'    => '_blank',
                    'rel'       => 'noreferrer',
                    'href'      => $file_details['url'],
                    'role'      => 'button',
                    'class'     => $btnclasss,
                    'text'   => $btntext,
                );
                break;

            case 'none':
                $link_url = '';
                $link_attr = array();
                $link_attr_image = array();
                break;
        }

        $loop_output[] = array(
            'sectionid'        => $section_count,
            'title'            => $title,
            'content'          => $content,
            'html'             => $html,
            'link_attr'        => $link_attr,
            'link_url'         => $link_url,
            'image_set'        => $image_set,
            'image'            => $cb_image,
            'image_url'            => $cb_image_url,
        );


    }

    $output = json_decode(json_encode($loop_output), FALSE);
    $bootstrap_classes = implode(' ', ws_get_boostrap_col($ID, array('prefix' => 'ws_content_block_')));

    $myrowcount = get_sub_field('ws_content_block_desktop_row');
}

?>

<?php

// SECTION START

if($full_width_background){
    if($block_background){
        echo "<div style='background:url(" . $block_background . ") no-repeat; background-size:cover;'>";
        echo "<div class='content__block " . $container_type . " "; if($block_extra_classes){ echo $block_extra_classes; } echo "'>";
    }
}
else {
    if($block_background){ echo "<div class='content__block " . $container_type . " "; if($block_extra_classes){ echo $block_extra_classes; } echo "' style='background:url(" . $block_background . ") no-repeat; background-size:cover;'>";
    }
    else {
        echo "<div class='content__block " . $container_type . " "; if($block_extra_classes){ echo $block_extra_classes; } echo "'>";
    }
}

?>

    <section>
        <div class='row <?php if($rowequalheight){ echo "row-eq-height"; } ?>'>

        <?php
        $block_count = 1;

        foreach ($output as $block) {
            if ($block->sectionid >= $section_count) {

                if($block->link_attr){
                    $block_button = "<a href='" . $block->link_attr->href . "' class='" . $block->link_attr->class . "' target='_blank'>" . $block->link_attr->text . "</a>";
                }
                else {
                    $block_button = "";
                }

                ?>


                <div class="block__item block__item-<?php echo $block_count; ?> <?php echo $bootstrap_classes; ?> <?php if ($block->extra_classes) { echo $block->extra_classes; } ?>">

                            <div class="block__item__content <?php if ($block->content_classes) { echo $block->content_classes; } ?>" <?php if ($block->background_image_set) { echo "style='" . $background_setting . "'"; } ?>>

                                <?php

                                if($block->html) {
                                    $myHTML = $block->html;
                                    $myHTML = str_replace("!BUTTON!",$block_button,$myHTML);
                                    $myHTML = str_replace("!SEC_IMAGE!",$block->sec_image_url,$myHTML);
                                    $myHTML = str_replace("!SUB_TITLE!",$block->subtitle,$myHTML);
                                    $myHTML = str_replace("!TITLE!",$block->title,$myHTML);
                                    $myHTML = str_replace("!CONTENT!",$block->content,$myHTML);
                                    $myHTML = str_replace("!FEATUREDURL!",$block->image_url,$myHTML);
                                    $myHTML = str_replace("!FEATURED!",$block->image,$myHTML);
                                    $myHTML = str_replace("!LINKURL!",$block->link_url,$myHTML);
                                    echo $myHTML;
                                }
                                elseif($itemshtml){
                                    $myHTML = $itemshtml;
                                    $myHTML = str_replace("!BUTTON!",$block_button,$myHTML);
                                    $myHTML = str_replace("!SEC_IMAGE!",$block->sec_image_url,$myHTML);
                                    $myHTML = str_replace("!SUB_TITLE!",$block->subtitle,$myHTML);
                                    $myHTML = str_replace("!TITLE!",$block->title,$myHTML);
                                    $myHTML = str_replace("!CONTENT!",$block->content,$myHTML);
                                    $myHTML = str_replace("!FEATUREDURL!",$block->image_url,$myHTML);
                                    $myHTML = str_replace("!FEATURED!",$block->image,$myHTML);
                                    $myHTML = str_replace("!LINKURL!",$block->link_url,$myHTML);
                                    //$myHTML = str_replace("!BACKGROUND!",$block->background,$myHTML);
                                    echo $myHTML;

                                }
                                else {
                                    if($block->title) {
                                        echo "<h2>" . $block->title . "</h2>";
                                    }

                                    if($block->content) {
                                        echo $block->content;
                                    }

                                    if($block->image_set) {
                                        echo $block->image;
                                    }

                                    if($block->link_url) {
                                        echo $block_button;
                                    }
                                }

                                ?>
                            </div>
                </div>


                <?php


                if($block_count%$myrowcount == 0){
                    if($block_count != count($output)){
                        echo "</div><div class='row row-eq-height'>";
                    }
                    else {
                        echo "</div>";
                    }
                }

                $block_count++;


            }

        }  // END FOREACH ?>


    </section>

</div>

<?php
// END FULL WIDTH BACKGROUND

if($full_width_background){
    if($block_background){ echo "</div>"; }
}
?>