<?php
/**
 * Template for showing Content Blocks
 */

if (!defined('ABSPATH'))
    exit; // Exit if accessed directly

$title = get_sub_field('ws_header_title');
$format = get_sub_field('ws_title_format');
$wrapper_class = get_sub_field('ws_title_wrapper_classes');

//$title_output['section_' .$section_count] = array(
//    'sectionid'        => 'section_' .$section_count,
//    'title'            => $title,
//    'format'         => $format,
//    'wrapper_class'    => $wrapper_class,
//);
//
////$title_output = json_decode(json_encode($title_output), FALSE);


?>


<div class="content__block container">
    <div class="row">
        <div class="col-xs-12 col-lg-12 title__item <?php echo $wrapper_class; ?>">
            <div class="about_text">
                <<?php echo $format; ?>><?php echo $title; ?></<?php echo $format; ?>>
                <span class="line_bar"></span>
            </div>
        </div>
    </div>
</div>





