<?php
/**
 * Template for showing Content Blocks
 */

if (!defined('ABSPATH'))
    exit; // Exit if accessed directly

$container = get_sub_field('ws_section_container_size');
$padding = get_sub_field('ws_section_padding');
$wrapper_class = get_sub_field('ws_section_wrapper_classes');

?>


<div class="section__break__block <?php echo $container; ?> <?php echo $wrapper_class; ?>" <?php if($padding){ echo 'style="padding: ' . $padding . ';"'; } ?>>
    <div class="row">
        <div class="col-xs-12">
        </div>
    </div>
</div>





