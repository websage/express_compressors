<?php
/**
 * Template for showing Testimonials
 */

if (!defined('ABSPATH'))
    exit; // Exit if accessed directly

// Global Variables
global $wp_query, $paged;

$block_extra_classes = get_sub_field('ws_testimonial_block_extra_classes');

if ( get_query_var('paged') ) { $paged = get_query_var('paged'); } else if ( get_query_var('page') ) {$paged = get_query_var('page'); } else {$paged = 1; }


if(get_sub_field('ws_order_type') == "random"){
// WP_Query arguments
$args = array(
    'post_type'              => array( 'testimonial' ),
    'order'                  => 'ASC',
    'orderby'                => 'menu_order',
    'paged'                  => $paged,
);

if(get_sub_field('ws_number_per_page')){
    $args['posts_per_page']=get_sub_field('ws_number_per_page');
    $args['_shuffle_and_pick']=get_sub_field('ws_number_per_page');
}

}
else {
    // WP_Query arguments
    $args = array(
        'post_type'              => array( 'testimonial' ),
        'order'                  => get_sub_field('ws_order_type'),
        'orderby'                => 'menu_order',
        'paged'                  => $paged,
    );


    if(get_sub_field('ws_number_per_page')){
        $args['posts_per_page']=get_sub_field('ws_number_per_page');
    }

}



// The Query
$query = new WP_Query( $args );

$bootstrap_classes = implode(' ', ws_get_boostrap_col($ID, array('prefix' => 'ws_testimonial_block_')));


?>


<div class="content__block <?php if($block_extra_classes){ echo $block_extra_classes; }  ?>">

    <section>
        <div class='row row-eq-height'>

        <?php

        $block_count = 1;

        // The Loop
        if ( $query->have_posts() ) {

            while ( $query->have_posts() ) {

                $query->the_post();

                ?>


                <div class="testimonial__item testimonial__item-<?php echo $block_count; ?> <?php echo $bootstrap_classes; ?>">

                    <div class="testimonial__item__content">

                        <?php

                        echo get_the_content();
                        echo "<p class='testimonial__title'>" . get_the_title() . "</p>";
                        echo "<p class='testimonial__location'>" . get_field('testimonial_sub_title') . "</p>";


                        ?>

                    </div>
                </div>


                <?php

                if ($block_count % 2 == 0) {
                    if ($block_count != $query->post_count) {
                        echo "</div><div class='row row-eq-height'>";
                    } else {
                        echo "</div>";
                    }
                }
                $block_count++;

            }

            echo "<nav class='paging'>";
            previous_posts_link( 'Previous Page' );
            next_posts_link( 'Next Page', $query->max_num_pages );
            echo "</nav>";



        } else {
            // no posts found
        }


        ?>

    </section>

</div>

<?php

// Restore original Post Data
wp_reset_postdata();

?>