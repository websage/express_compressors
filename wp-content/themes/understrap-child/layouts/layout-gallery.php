<?php
/**
 * Template for displaying Gallery
 */
if (!defined('ABSPATH'))
    exit; // Exit if accessed directly

$bootstrap_classes = implode(' ', ws_get_boostrap_col($ID, array('prefix' => 'ws_gallery_')));

$images = get_sub_field('ws_gallery_images');

if( $images ): ?>

<div class="galley__block container">
    <div class="row">
        <?php foreach( $images as $image ): ?>
        <div class="<?php echo $bootstrap_classes; ?> gallery__item">
            <a href="<?php echo $image['url']; ?>" class="gallery">
                <?php echo ws_get_image( array("type"=>"img-responsive","id"=>$image['id'],"size"=>'gallery_thumb', "lightbox"=>'true'));  ?>
            </a>
        </div>
        <?php endforeach; ?>
    </div>
</div>



<?php endif; ?>
