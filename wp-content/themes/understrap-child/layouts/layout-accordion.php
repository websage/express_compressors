<?php
/**
 * Template for displaying Gallery
 */
if (!defined('ABSPATH'))
    exit; // Exit if accessed directly


?>
<div class="accordion__block container-fluid">
    <div class="container">
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-xs-12 col-sm-12 col-md-10">
                <div id="accordion">
                    <?php

                    // check if the repeater field has rows of data
                    if (have_rows('ws_accordion_item')):

                        // loop through the rows of data
                        while (have_rows('ws_accordion_item')) : the_row();

                            ?>

                            <h4 class="accordion-toggle"><?php the_sub_field('ws_accordion_title');  ?></h4>
                            <div class="accordion-content">
                                <?php  the_sub_field('ws_accordion_content'); ?>
                                <?php if(get_sub_field('ws_accordion_file')){
                                    echo "<a href='" . get_sub_field('ws_accordion_file') . "' class='btn btn-primary'>Download</a>";
                                }   ?>
                            </div>

                            <?php


                        endwhile;

                    endif;

                    ?>

                </div>

            </div>
            <div class="col-md-1"></div>

        </div>
    </div>
</div>

<script type="text/javascript">

</script>
