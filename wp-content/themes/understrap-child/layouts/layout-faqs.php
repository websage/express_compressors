<?php
/**
 * Template for showing FAQs
 */

if (!defined('ABSPATH'))
    exit; // Exit if accessed directly

$container = get_theme_mod('understrap_container_type');

$faqs = new WP_Query(array(
    'post_type' => 'faqs',
    'orderby' => 'menu_order',
    'order' => 'ASC',
    'nopaging' => true,
));

?>

<?php

$i = 100;

if ($faqs->have_posts()) {
    ?>

    <div class="panel-group faq__listing" id="accordion">

        <?php

        while ($faqs->have_posts()) : $faqs->the_post();

            ?>

            <div class="panel panel-default">
                <div class="panel-heading collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $i;  ?>">
                    <h4 class="panel-title">
                        <a class="accordion-toggle"><?php echo get_the_title(); ?></a>
                    </h4>
                </div>
                <div id="collapse<?php echo $i;  ?>" class="panel-collapse collapse">
                    <div class="panel-body">
                        <?php echo get_the_content(); ?>
                    </div>
                </div>
            </div>

            <?php

            $i++;

        endwhile; //WHILE
        wp_reset_query();
        ?>
    </div>
    <?php

} //IF


?>