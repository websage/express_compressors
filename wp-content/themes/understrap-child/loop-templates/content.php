<?php
/**
 * Post rendering content according to caller of get_template_part.
 *
 * @package understrap
 */

?>
<div class="col-sm-12 col-md-4">
    <div class="text-box blog-archive">
<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">



	<a href="<?php echo get_permalink(); ?>"><?php echo ws_get_image( array("type"=>"img-responsive","id"=>get_post_thumbnail_id( $post->ID ),"size"=>'newslisting', "title" => get_post_meta(get_post_thumbnail_id(), '_wp_attachment_image_alt', true)));  ?></a>

    <header class="entry-header">

        <?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ),
            '</a></h2>' ); ?>

    </header><!-- .entry-header -->

	<div class="entry-content">

		<?php
		the_excerpt();
		?>

		<?php
		wp_link_pages( array(
			'before' => '<div class="page-links">' . __( 'Pages:', 'understrap' ),
			'after'  => '</div>',
		) );
		?>

	</div><!-- .entry-content -->

</article><!-- #post-## -->
</div></div>
