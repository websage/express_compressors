<?php
/**
 * Used to Display the FAQ's in the Footer
 */

$container = get_theme_mod('understrap_container_type');

$faqs = new WP_Query(array(
    'post_type' => 'faqs',
    'orderby' => 'menu_order',
    'order' => 'ASC',
));

?>

<div class="col-md-4">
    <h2>FAQ</h2>

    <?php

    $i = 1;

    if ($testimonial->have_posts()) {
        ?>

        <div class="panel-group" id="accordion">

            <?php

            while ($testimonial->have_posts()) : $testimonial->the_post();

                ?>

                <div class="panel panel-default">
                    <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                        <h4 class="panel-title">
                            <a class="accordion-toggle">
                                <?php echo get_the_title(); ?>
                            </a>
                        </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <?php echo get_the_content(); ?>
                        </div>
                    </div>
                </div>

                <?php

                $i++;

            endwhile; //WHILE
            wp_reset_query();
            ?>
        </div>
        <?php

    } //IF


    ?>

</div>
</div>
