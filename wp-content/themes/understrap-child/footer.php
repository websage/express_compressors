<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */

$the_theme = wp_get_theme();
$container = get_theme_mod('understrap_container_type');
$companyname = get_field('ws_business_name', 'options');

?>

<!-- PRE-FOOTER START -->

<div>
    <div class="container">
        <section>
            <div class="row">
                <div class="col-md-12">

                </div>
            </div>
        </section>
    </div>
</div>

<!-- PRE-FOOTER END -->

<!-- FOOTER START -->

<div>
    <div class="container">
        <section>
            <div class="row">
                <div class="col-md-12">

                </div>
            </div>
        </section>
    </div>
</div>



<!-- FOOTER END -->
</div>


<?php wp_footer(); ?>

<script>
    jQuery(document).ready(function(){
        console.log("test");
        jQuery('a.gallery').featherlightGallery({
            previousIcon: '&#9664;',     /* Code that is used as previous icon */
            nextIcon: '&#9654;',         /* Code that is used as next icon */
            galleryFadeIn: 100,          /* fadeIn speed when slide is loaded */
            galleryFadeOut: 300          /* fadeOut speed before slide is loaded */
        });
    });
</script>



</body>

</html>
